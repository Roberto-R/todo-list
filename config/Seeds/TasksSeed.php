<?php
use Migrations\AbstractSeed;

/**
 * Tasks seed.
 */
class TasksSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '2',
                'category_id' => '2',
                'name' => 'Do laundry',
                'completed' => '2020-04-23 12:39:51',
                'description' => NULL,
                'priority' => '0',
                'created' => '2020-04-23 09:54:41',
                'modified' => '2020-04-23 12:39:51',
            ],
            [
                'id' => '3',
                'category_id' => '1',
                'name' => 'Fix flex',
                'completed' => '2020-04-23 12:36:18',
                'description' => NULL,
                'priority' => '0',
                'created' => '2020-04-23 10:21:35',
                'modified' => '2020-04-23 12:36:18',
            ],
            [
                'id' => '5',
                'category_id' => '3',
                'name' => 'Buy clothes',
                'completed' => '2020-04-23 12:39:54',
                'description' => NULL,
                'priority' => '0',
                'created' => '2020-04-23 10:41:11',
                'modified' => '2020-04-23 12:39:54',
            ],
            [
                'id' => '6',
                'category_id' => '3',
                'name' => 'Get a new laptop',
                'completed' => '2020-04-23 12:39:55',
                'description' => NULL,
                'priority' => '0',
                'created' => '2020-04-23 10:41:20',
                'modified' => '2020-04-23 12:39:55',
            ],
            [
                'id' => '7',
                'category_id' => '2',
                'name' => 'Fix leaking sink',
                'completed' => NULL,
                'description' => 'Sink is leaking, I need to fix it.',
                'priority' => '2',
                'created' => '2020-04-23 15:04:39',
                'modified' => '2020-04-23 16:52:46',
            ],
            [
                'id' => '8',
                'category_id' => '2',
                'name' => 'Iron shirts',
                'completed' => '2020-04-23 16:02:32',
                'description' => NULL,
                'priority' => '3',
                'created' => '2020-04-23 15:04:48',
                'modified' => '2020-04-23 16:02:32',
            ],
            [
                'id' => '9',
                'category_id' => '2',
                'name' => 'Do something else',
                'completed' => NULL,
                'description' => NULL,
                'priority' => '1',
                'created' => '2020-04-23 15:54:24',
                'modified' => '2020-04-23 17:23:52',
            ],
            [
                'id' => '10',
                'category_id' => '3',
                'name' => 'Buy some fruit',
                'completed' => NULL,
                'description' => NULL,
                'priority' => '0',
                'created' => '2020-04-23 16:00:38',
                'modified' => '2020-04-23 16:00:38',
            ],
            [
                'id' => '11',
                'category_id' => '7',
                'name' => 'Make an application',
                'completed' => NULL,
                'description' => NULL,
                'priority' => '3',
                'created' => '2020-04-23 17:05:43',
                'modified' => '2020-04-23 17:05:43',
            ],
            [
                'id' => '12',
                'category_id' => '7',
                'name' => 'Task 1',
                'completed' => '2020-04-23 17:07:55',
                'description' => NULL,
                'priority' => '0',
                'created' => '2020-04-23 17:07:35',
                'modified' => '2020-04-23 17:07:55',
            ],
            [
                'id' => '13',
                'category_id' => '7',
                'name' => 'Task 2',
                'completed' => '2020-04-23 17:07:54',
                'description' => NULL,
                'priority' => '0',
                'created' => '2020-04-23 17:07:39',
                'modified' => '2020-04-23 17:07:54',
            ],
            [
                'id' => '14',
                'category_id' => '7',
                'name' => 'Task 3',
                'completed' => '2020-04-23 17:07:53',
                'description' => NULL,
                'priority' => '0',
                'created' => '2020-04-23 17:07:43',
                'modified' => '2020-04-23 17:07:53',
            ],
            [
                'id' => '15',
                'category_id' => '7',
                'name' => 'Task 4',
                'completed' => '2020-04-23 17:07:51',
                'description' => NULL,
                'priority' => '0',
                'created' => '2020-04-23 17:07:47',
                'modified' => '2020-04-23 17:07:51',
            ],
            [
                'id' => '16',
                'category_id' => '8',
                'name' => 'Watchmen',
                'completed' => NULL,
                'description' => NULL,
                'priority' => '0',
                'created' => '2020-04-23 17:25:33',
                'modified' => '2020-04-23 17:25:33',
            ],
            [
                'id' => '17',
                'category_id' => '8',
                'name' => 'The Dark Knight',
                'completed' => NULL,
                'description' => NULL,
                'priority' => '0',
                'created' => '2020-04-23 17:25:42',
                'modified' => '2020-04-23 17:25:42',
            ],
            [
                'id' => '18',
                'category_id' => '8',
                'name' => '1917',
                'completed' => NULL,
                'description' => NULL,
                'priority' => '0',
                'created' => '2020-04-23 17:25:48',
                'modified' => '2020-04-23 17:25:48',
            ],
        ];

        $table = $this->table('tasks');
        $table->insert($data)->save();
    }
}
