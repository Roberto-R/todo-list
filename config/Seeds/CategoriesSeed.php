<?php
use Migrations\AbstractSeed;

/**
 * Categories seed.
 */
class CategoriesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Work',
                'description' => NULL,
                'colour' => 'FFFFFF',
                'priority' => '0',
                'deleted' => '2020-04-23 12:40:14',
                'created' => '2020-04-23 09:53:34',
                'modified' => '2020-04-23 12:40:14',
            ],
            [
                'id' => '2',
                'name' => 'House',
                'description' => NULL,
                'colour' => 'cce6e6',
                'priority' => '0',
                'deleted' => NULL,
                'created' => '2020-04-23 09:53:55',
                'modified' => '2020-04-23 15:35:14',
            ],
            [
                'id' => '3',
                'name' => 'Shopping',
                'description' => NULL,
                'colour' => '97ffcb',
                'priority' => '0',
                'deleted' => NULL,
                'created' => '2020-04-23 09:14:08',
                'modified' => '2020-04-23 15:35:25',
            ],
            [
                'id' => '7',
                'name' => 'Work',
                'description' => NULL,
                'colour' => 'dddddd',
                'priority' => '0',
                'deleted' => NULL,
                'created' => '2020-04-23 17:05:30',
                'modified' => '2020-04-23 17:05:30',
            ],
            [
                'id' => '8',
                'name' => 'Movies',
                'description' => NULL,
                'colour' => '5bffff',
                'priority' => '0',
                'deleted' => NULL,
                'created' => '2020-04-23 17:25:24',
                'modified' => '2020-04-23 17:25:24',
            ],
        ];

        $table = $this->table('categories');
        $table->insert($data)->save();
    }
}
