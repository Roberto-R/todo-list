<?php
use Migrations\AbstractSeed;

/**
 * Tags seed.
 */
class TagsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '3',
                'name' => 'Cool',
                'description' => '',
                'colour' => '800080',
                'created' => '2020-04-23 16:52:35',
                'modified' => '2020-04-23 16:52:35',
            ],
            [
                'id' => '4',
                'name' => 'Important',
                'description' => '',
                'colour' => 'ff8040',
                'created' => '2020-04-23 17:01:07',
                'modified' => '2020-04-23 17:01:07',
            ],
            [
                'id' => '5',
                'name' => 'Optional',
                'description' => '',
                'colour' => '95ff95',
                'created' => '2020-04-23 17:23:42',
                'modified' => '2020-04-23 17:23:42',
            ],
        ];

        $table = $this->table('tags');
        $table->insert($data)->save();
    }
}
