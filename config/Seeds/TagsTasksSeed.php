<?php
use Migrations\AbstractSeed;

/**
 * TagsTasks seed.
 */
class TagsTasksSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '3',
                'tag_id' => '3',
                'task_id' => '7',
                'created' => '2020-04-23 16:52:46',
            ],
            [
                'id' => '4',
                'tag_id' => '3',
                'task_id' => '9',
                'created' => '2020-04-23 16:52:52',
            ],
            [
                'id' => '5',
                'tag_id' => '3',
                'task_id' => '11',
                'created' => '2020-04-23 17:05:43',
            ],
            [
                'id' => '6',
                'tag_id' => '4',
                'task_id' => '11',
                'created' => '2020-04-23 17:05:43',
            ],
            [
                'id' => '7',
                'tag_id' => '5',
                'task_id' => '9',
                'created' => '2020-04-23 17:23:52',
            ],
            [
                'id' => '8',
                'tag_id' => '3',
                'task_id' => '16',
                'created' => '2020-04-23 17:25:33',
            ],
        ];

        $table = $this->table('tags_tasks');
        $table->insert($data)->save();
    }
}
