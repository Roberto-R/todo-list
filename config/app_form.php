<?php

return [
    //'checkbox' => '<input type="checkbox" name="{{name}}" class="form-check-input" value="{{value}}"{{attrs}}>', // Ugly results in actions form
    'checkboxWrapper' => '<div class="form-check checkbox">{{label}}</div>',
    'input' => '<input type="{{type}}" name="{{name}}" class="form-control"{{attrs}}/>',
    'textarea' => '<textarea name="{{name}}" class="form-control"{{attrs}}>{{value}}</textarea>',
    'select' => '<select name="{{name}}" class="form-control"{{attrs}}>{{content}}</select>',
    'selectMultiple' => '<select name="{{name}}[]" class="form-control" multiple="multiple"{{attrs}}>{{content}}</select>',
    'submitContainer' => '{{content}}',
    'inputSubmit' => '<input class="btn btn-primary" type="{{type}}"{{attrs}}/>',
];
