<?php

use Migrations\AbstractMigration;

class Initial extends AbstractMigration
{

    public function up()
    {

        $categories = $this->table('categories');
        $categories
                ->addColumn('name', 'string', [
                    'default' => null,
                    'limit' => 200,
                    'null' => false,
                ])
                ->addColumn('description', 'text', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addColumn('colour', 'string', [
                    'default' => 'FFFFFF',
                    'limit' => 6,
                    'null' => false,
                ])
                ->addColumn('priority', 'integer', [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                    'signed' => false,
                ])
                ->addColumn('deleted', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addColumn('created', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addColumn('modified', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->create();

        $tags = $this->table('tags');
        $tags->addColumn('name', 'string', [
                    'default' => null,
                    'limit' => 200,
                    'null' => false,
                ])
                ->addColumn('description', 'text', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addColumn('colour', 'string', [
                    'default' => null,
                    'limit' => 6,
                    'null' => false,
                ])
                ->addColumn('created', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addColumn('modified', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->create();

        $tags_tasks = $this->table('tags_tasks');
        $tags_tasks
                ->addColumn('tag_id', 'integer', [
                    'default' => null,
                    'limit' => 11,
                    'null' => false,
                ])
                ->addColumn('task_id', 'integer', [
                    'default' => null,
                    'limit' => 11,
                    'null' => false,
                ])
                ->addColumn('created', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addIndex('tag_id')
                ->addIndex('task_id')
                ->addIndex(['tag_id', 'task_id'], ['unique' => true])
                ->create();

        $tasks = $this->table('tasks');
        $tasks->addColumn('category_id', 'integer', [
                    'default' => null,
                    'limit' => 11,
                    'null' => false,
                ])
                ->addColumn('name', 'string', [
                    'default' => null,
                    'limit' => 200,
                    'null' => false,
                ])
                ->addColumn('completed', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addColumn('description', 'text', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addColumn('priority', 'integer', [
                    'default' => '0',
                    'limit' => 10,
                    'null' => false,
                    'signed' => false,
                ])
                ->addColumn('created', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addColumn('modified', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addIndex(
                        [
                            'category_id',
                        ]
                )
                ->create();

        $tags_tasks->addForeignKey(
                        'tag_id',
                        'tags',
                        'id',
                        [
                            'update' => 'RESTRICT',
                            'delete' => 'RESTRICT'
                        ]
                )
                ->addForeignKey(
                        'task_id',
                        'tasks',
                        'id',
                        [
                            'update' => 'RESTRICT',
                            'delete' => 'RESTRICT'
                        ]
                )
                ->update();

        $tasks->addForeignKey(
                        'category_id',
                        'categories',
                        'id',
                        [
                            'update' => 'RESTRICT',
                            'delete' => 'RESTRICT'
                        ]
                )
                ->update();
    }

    public function down()
    {
        $this->table('tags_tasks')
                ->dropForeignKey(
                        'tag_id'
                )
                ->dropForeignKey(
                        'task_id'
                )->save();

        $this->table('tasks')
                ->dropForeignKey(
                        'category_id'
                )->save();

        $this->table('categories')->drop()->save();
        $this->table('tags')->drop()->save();
        $this->table('tags_tasks')->drop()->save();
        $this->table('tasks')->drop()->save();
    }

}
