<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CategoriesFixture
 */
class CategoriesFixture extends TestFixture
{
    /**
     * Import from default database
     *
     * @var array
     */
    public $import = ['table' => 'categories'];
    
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            1 => [
                'name' => 'My first category',
                'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'colour' => 'FFFFFF',
                'priority' => 1,
                'deleted' => null,
                'created' => '2020-04-23 07:16:34',
                'modified' => '2020-04-23 07:16:34',
            ],
            2 => [
                'name' => 'Empty category',
                'colour' => 'FFFFFF',
                'priority' => 2,
                'deleted' => null,
                'created' => '2020-04-23 07:16:34',
                'modified' => '2020-04-23 07:16:34',
            ],
        ];
        parent::init();
    }
}
