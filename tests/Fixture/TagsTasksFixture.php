<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TagsTasksFixture
 */
class TagsTasksFixture extends TestFixture
{

    /**
     * Import from default database
     *
     * @var array
     */
    public $import = ['table' => 'tags_tasks'];

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            1 => [
                'tag_id' => 1,
                'task_id' => 1,
                'created' => '2020-04-23 07:49:36',
            ],
        ];
        parent::init();
    }

}
