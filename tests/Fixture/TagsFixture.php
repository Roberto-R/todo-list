<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TagsFixture
 */
class TagsFixture extends TestFixture
{
    /**
     * Import from default database
     *
     * @var array
     */
    public $import = ['table' => 'tags'];
    
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            1 => [
                'name' => 'Lorem ipsum dolor sit amet',
                'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'colour' => 'Lore',
                'created' => '2020-04-23 07:16:38',
                'modified' => '2020-04-23 07:16:38',
            ],
        ];
        parent::init();
    }
}
