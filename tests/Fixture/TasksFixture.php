<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TasksFixture
 */
class TasksFixture extends TestFixture
{
        /**
     * Import from default database
     *
     * @var array
     */
    public $import = ['table' => 'tasks'];
    
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            1 => [
                'category_id' => 1,
                'name' => 'Lorem ipsum dolor sit amet',
                'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'priority' => 1,
                'completed' => null,
                'created' => '2020-04-23 07:16:28',
                'modified' => '2020-04-23 07:16:28',
            ],
            2 => [
                'category_id' => 1,
                'name' => 'First completed task',
                'description' => 'This task is completed',
                'priority' => 1,
                'completed' => '2020-04-23 05:16:28',
                'created' => '2020-04-23 07:16:28',
                'modified' => '2020-04-23 07:16:28',
            ],
            3 => [
                'category_id' => 1,
                'name' => 'Another open task',
                'priority' => 0,
                'completed' => null,
                'created' => '2020-04-23 07:16:28',
                'modified' => '2020-04-23 07:16:28',
            ],
            4 => [
                'category_id' => 1,
                'name' => 'Do some stuff',
                'priority' => 1,
                'completed' => null,
                'created' => '2020-04-23 07:16:28',
                'modified' => '2020-04-23 07:16:28',
            ],
        ];
        parent::init();
    }
}
