<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CategoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CategoriesTable Test Case
 */
class CategoriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CategoriesTable
     */
    public $Categories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Categories',
        'app.Tasks',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Categories') ? [] : ['className' => CategoriesTable::class];
        $this->Categories = TableRegistry::getTableLocator()->get('Categories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Categories);

        parent::tearDown();
    }

    /**
     * Test the tasksCount finder
     */
    public function testFindTasksCount()
    {
        $id = 1;
        $category = $this->Categories->get($id, [
            'finder' => 'tasksCount'
        ]);

        $this->assertArrayHasKey('tasks_completed', $category->toArray());
        $this->assertArrayHasKey('tasks_open', $category->toArray());
        
        $this->assertEquals(1, $category->tasks_completed);
        $this->assertEquals(3, $category->tasks_open);
    }

}
