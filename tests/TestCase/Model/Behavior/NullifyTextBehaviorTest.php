<?php

namespace App\Test\TestCase\Model\Behavior;

use App\Model\Behavior\NullifyTextBehavior;
use Cake\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Model\Behavior\NullifyTextBehavior Test Case
 * 
 * @property \App\Model\Table\CategoriesTable $Categories
 */
class NullifyTextBehaviorTest extends TestCase
{

    public $Categories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Categories'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Categories = TableRegistry::getTableLocator()->get('Categories');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Categories);

        parent::tearDown();
    }

    /**
     * Test behaviour
     */
    public function testMarshal()
    {
        $data = [
            'name' => 'New category',
            'colour' => 'FFFFFF',
            'description' => ''
        ];

        $category_new = $this->Categories->newEntity($data);
        $this->Categories->saveOrFail($category_new);

        $category = $this->Categories->find()
                ->where(['name' => $data['name']])
                ->order(['created' => 'desc'])
                ->first();

        // Make sure it's null, not ''
        $this->assertNull($category->description);
    }

}
