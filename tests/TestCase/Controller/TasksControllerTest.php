<?php

namespace App\Test\TestCase\Controller;

use App\Controller\TasksController;
use Cake\I18n\Time;

/**
 * App\Controller\TasksController Test Case
 *
 * @uses \App\Controller\TasksController
 */
class TasksControllerTest extends ControllerTestBase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Categories',
        'app.Tasks',
        'app.Tags',
        'app.TagsTasks',
    ];
    
    public $alias = 'Tasks';

    /**
     * Test index method
     */
    public function testIndex()
    {
        $this->get("/tasks/index");
        $this->assertResponseOk();
    }
    
    /**
     * Test archive method
     */
    public function testIndexArchive()
    {
        $this->get("/tasks/index-archive");
        $this->assertResponseOk();
    }

    /**
     * Test edit method for new object
     */
    public function testAdd()
    {
        $this->get("/tasks/edit");

        $this->assertResponseOk();

        $data = [
            'name' => 'Cool new task!',
            'description' => '',
            'category_id' => 1
        ];

        $this->post("/tasks/edit", $data);
        $this->assertRedirect();
    }

    /**
     * Test edit method for existing object
     */
    public function testEdit()
    {
        $id = 1;
        
        $this->get("/tasks/edit/{$id}");

        $this->assertResponseOk();

        $data = [
            'name' => 'Edited name'
        ];

        $this->post("/tasks/edit/{$id}", $data);
        $this->assertRedirect();
        
        $this->loadModel();
        
        $task = $this->Tasks->get($id);
        
        $this->assertArraySubset($data, $task->toArray());
    }
    
    /**
     * Test delete action
     */
    public function testDelete()
    {
        $id = 1;
        
        $this->post("/tasks/delete/{$id}");
        $this->assertRedirect();
        
        $this->loadModel();
        
        $task = $this->Tasks->find()
                ->where(compact('id'))
                ->first();
        
        $this->assertNull($task);
    }
    
    /**
     * Search search method
     */
    public function testSearch()
    {
        $this->get("/tasks/search.json?term=Lor");
        
        $this->assertResponseOk();
        
        $result = $this->getJsonResponse();
        
        $this->assertNotEmpty($result);
        
        $found = false;
        foreach ($result['tasks'] as $task)
        {
            if ($task['id'] == 1)
            {
                $found = true;
            }
        }
        $this->assertTrue($found);
    }
    
    /**
     * Test complete method
     */
    public function testComplete()
    {
        $id = 1;
        
        $this->post("/tasks/complete/{$id}");
        
        $this->assertRedirect();
        
        $this->loadModel();
        
        $task = $this->Tasks->get($id);
        
        $this->assertNotNull($task->completed);
        $now = (new Time())->getTimestamp();
        $this->assertEquals($now, $task->completed->getTimestamp(), '', 3);
    }

}
