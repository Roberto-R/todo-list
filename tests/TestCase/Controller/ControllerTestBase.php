<?php

namespace App\Test\TestCase\Controller;

use Cake\TestSuite\TestCase;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\ORM\TableRegistry;

/**
 * Shared base class for controller tests
 */
class ControllerTestBase extends TestCase
{

    use IntegrationTestTrait;
    
    /**
     * Table name
     * 
     * @var string
     */
    public $alias = null;

    /**
     * Run before each test
     * 
     * @return void
     */
    public function setUp(): void
    {
        $this->enableCsrfToken();


        parent::setUp();
    }
    
    /**
     * Create table instance
     * 
     * Use alias or fallback on controller alias
     */
    protected function loadModel($alias = null)
    {
        if (!$alias)
        {
            $alias = $this->alias;
        }
        
        if (!$alias)
        {
            trigger_error("Table alias was not defined for this test");
        }
        
        $this->{$alias} = TableRegistry::getTableLocator()->get($alias);
    }
    
    /**
     * Turn JSON response into array
     * 
     * @return array
     */
    public function getJsonResponse()
    {
        $body = (string)$this->_response->getBody();
        return json_decode($body, true); // Create array
    }

}
