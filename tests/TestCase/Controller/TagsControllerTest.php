<?php
namespace App\Test\TestCase\Controller;

use App\Controller\TagsController;

/**
 * App\Controller\TagsController Test Case
 *
 * @uses \App\Controller\TagsController
 */
class TagsControllerTest extends ControllerTestBase
{
    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Tags',
        'app.Tasks',
        'app.TagsTasks',
    ];

    public $alias = 'Tags';
    
    /**
     * Test index method
     */
    public function testIndex()
    {
        $this->get("/tags/index");
        $this->assertResponseOk();
    }

    /**
     * Test edit method for new object
     */
    public function testAdd()
    {
        $this->get("/tags/edit");

        $this->assertResponseOk();

        $data = [
            'name' => 'This is a new tag!',
            'colour' => 'FF0000'
        ];

        $this->post("/tags/edit", $data);
        $this->assertRedirect();
    }

    /**
     * Test edit method for existing object
     */
    public function testEdit()
    {
        $id = 1;
        
        $this->get("/tags/edit/{$id}");

        $this->assertResponseOk();

        $data = [
            'name' => 'Edited name'
        ];

        $this->post("/tags/edit/{$id}", $data);
        $this->assertRedirect();
        
        $this->loadModel();
        
        $tag = $this->Tags->get($id);
        
        $this->assertArraySubset($data, $tag->toArray());
    }
    
    /**
     * Test delete action
     */
    public function testDelete()
    {
        $id = 1;
        
        $this->post("/tags/delete/{$id}");
        $this->assertRedirect();
        
        $this->loadModel();
        
        $task = $this->Tags->find()
                ->where(compact('id'))
                ->first();
        
        $this->assertNull($task);
    }

}
