<?php

namespace App\Test\TestCase\Controller;

use App\Controller\CategoriesController;

/**
 * App\Controller\CategoriesController Test Case
 *
 * @uses \App\Controller\CategoriesController
 * @property \App\Model\Table\CategoriesTable $Categories
 */
class CategoriesControllerTest extends ControllerTestBase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Categories',
        'app.Tasks',
        'app.Tags',
        'app.TagsTasks'
    ];
    
    public $alias = 'Categories';

    /**
     * Test edit method for new object
     *
     * @return void
     */
    public function testAdd()
    {
        $this->get("/categories/edit");

        $this->assertResponseOk();

        $data = [
            'name' => 'Cool new category!',
            'description' => '',
            'colour' => 'FFFFFF'
        ];

        $this->post("/categories/edit", $data);
        $this->assertRedirect();
    }

    /**
     * Test edit method for existing object
     *
     * @return void
     */
    public function testEdit()
    {
        $id = 1;
        
        $this->get("/categories/edit/{$id}");

        $this->assertResponseOk();

        $data = [
            'name' => 'Edited name'
        ];

        $this->post("/categories/edit/{$id}", $data);
        $this->assertRedirect();
        
        $this->loadModel();
        
        $category = $this->Categories->get($id);
        
        $this->assertArraySubset($data, $category->toArray());
    }
    
    /**
     * Test simple hard delete
     */
    public function testDeleteHard()
    {
        $id = 2;
        
        $this->post("/categories/delete/{$id}");
        $this->assertRedirect();
        
        $this->loadModel();
        
        $category = $this->Categories->find('all', ['archive' => true])
                ->where(compact('id'))
                ->first();
        
        $this->assertNull($category);
    }
    
    /**
     * Test soft delete (when archived tasks need to be kept)
     */
    public function testDeleteSoft()
    {
        $id = 1;
        
        $this->post("/categories/delete/{$id}");
        $this->assertRedirect();
        
        $this->loadModel();
        
        $category = $this->Categories->find('tasksCount', ['archive' => true])
                ->where(['Categories.id' => $id])
                ->first();
        
        $this->assertNotNull($category); // Must still exist
        
        $this->assertEquals(0, $category->tasks_open); // Open tasks should have been deleted
    }

}
