# TODO LIST
 
A simple application to track tasks for a single user.
 
Built using: **CakePHP 3.x** [![Build Status](https://img.shields.io/travis/cakephp/app/master.svg?style=flat-square)](https://travis-ci.org/cakephp/app) [![Total Downloads](https://img.shields.io/packagist/dt/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)
 
## Getting Started
 
### Prerequisites
 
A web server with PHP7 is required, PHP7.2 or higher is recommended.  
Consult the CakePHP documentation for all requirements.
 
### Installing
 
1. Clone repository.
2. Build dependencies by running `composer update -o`, or `composer install -o --no-dev` for production.
3. Create an empty database or acquire credentials to an existing one.
4. Set configuration in the `config/` folder. Copy `app.default.php` to `app.php` and change the settings. The datasource needs to be set at minimum.
4. Visit `/pages/home` in your browser to verify everything is working. Fix issues if they arise.
5. Build the database by running `bin/cake migrations migrate`. Optionally, you can seed the database by running `bin/cake migrations seed`.
 
## Running the tests
 
Run `# ./vendor/bin/phpunit` to run unit tests. Note that dependencies need to have been built with `--dev` for this to work.  
Specify specif test cases like `# ./vendor/bin/phpunit ./tests/TestCases/Controller/UsersControllerTest.php`. Run only specific tests by added `--filter [regEx test function name]`.
 
## Deployment
 
See the *Installing* section. Mind the use of `--no-dev`.
 
## Migrations and Fixtures
 
Bake the current database with:
```
# bin/cake bake migrations_snapshot Initial
```
 
Write the latest migration to the database:
```
# bin/cake migrations migrate
```
 
Bake a new seed file, based on existing data:
```
# bin/cake bake seed --data Users
```
 
Seed the current database:
```
# bin/cake migrations seed --seed UsersSeed
```
 
Bake fixtures based on the current database:
```
# bin/cake bake fixtures Users --records --count 1000
```
 
 
## Built With
 
* [CakePHP](http://cakephp.com) - CakePHP 3.8
 
## Authors
 
* **Robert Roos** - Lead development - (robert.soor@gmail.com)
 
## License
 
This project is owned by Robert Roos.
