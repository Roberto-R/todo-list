<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;

/**
 * Tags Model
 *
 * @property \App\Model\Table\TasksTable&\Cake\ORM\Association\BelongsToMany $Tasks
 *
 * @method \App\Model\Entity\Tag get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tag newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tag[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tag|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tag saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tag patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tag[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tag findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TagsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tags');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Tasks');
        $this->hasMany('TagsTasks')
                ->setDependent(true); // Enable cascading delete
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->scalar('name')
                ->maxLength('name', 200)
                ->requirePresence('name', 'create')
                ->notEmptyString('name');

        $validator
                ->scalar('description')
                ->allowEmptyString('description');

        $validator
                ->scalar('colour')
                ->maxLength('colour', 6)
                ->requirePresence('colour', 'create')
                ->notEmptyString('colour');

        return $validator;
    }

    /**
     * Parse data before entity creation
     * 
     * @param Event $event
     * @param ArrayObject $data
     * @param ArrayObject $options
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        if (isset($data['colour']))
        {
            $hex = strtolower($data['colour']);
            $data['colour'] = preg_replace('/[^a-f0-9?!]/', '', $hex);
        }
    }

    /**
     * Finder for tags including count of tasks
     * 
     * @param Query $query
     * @param array $options
     */
    public function findTasksCount(Query $query, array $options)
    {
        return $query
                        ->select($this)
                        ->select(['count_tasks' => 'COUNT(TagsTasks.tag_id)'])
                        ->leftJoinWith('Tasks', function ($q) {
                            return $q->find('open');
                        })
                        ->group('Tags.id');
    }

}
