<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use App\Model\Entity\Category;
use ArrayObject;

/**
 * Categories Model
 *
 * @property \App\Model\Table\TasksTable&\Cake\ORM\Association\HasMany $Tasks
 *
 * @method \App\Model\Entity\Category get($primaryKey, $options = [])
 * @method \App\Model\Entity\Category newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Category[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Category|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Category saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Category patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Category[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Category findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CategoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('categories');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('NullifyText', ['fields' => ['description']]);

        $this->hasMany('Tasks')
                ->setDependent(true); // Enable cascading delete
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->scalar('name')
                ->maxLength('name', 200)
                ->requirePresence('name', 'create')
                ->notEmptyString('name');

        $validator
                ->scalar('description')
                ->allowEmptyString('description');

        $validator
                ->scalar('colour')
                ->lengthBetween('colour', [6, 6])
                ->notEmptyString('colour');

        $validator
                ->nonNegativeInteger('priority')
                ->notEmptyString('priority');

        $validator
                ->scalar('deleted')
                ->datetime('deleted');

        return $validator;
    }

    /**
     * Callback before finder is executed
     * 
     * @param Event $event
     * @param Query $query
     * @param ArrayObject $options
     * @param boolean $primary
     */
    public function beforeFind(Event $event, Query $query, ArrayObject $options, bool $primary)
    {
        if (isset($options['archive']) && $options['archive'])
        {
            return $query; // Do nothing
        }

        return $query->where('Categories.deleted IS NULL');
    }

    /**
     * Event fired before saving
     * 
     * @param Event $event
     * @param Category $category
     * @param ArrayObject $options
     */
    public function beforeSave(Event $event, Category $category, ArrayObject $options)
    {
        if ($category->isDirty('deleted') && $category->is_deleted)
        {
            /*
             * Delete open tasks when category is soft deleted
             */

            $open_tasks = $this->Tasks->find()
                    ->where('Tasks.completed IS NULL')
                    ->where(['Tasks.category_id' => $category->id]);

            foreach ($open_tasks as $task)
            {
                if (!$this->Tasks->delete($task))
                {
                    $category->setError('tasks', 'Failed to delete open tasks');
                    return false; // Abort
                }
            }
        }
    }

    /**
     * Parse data before entity creation
     * 
     * @param Event $event
     * @param ArrayObject $data
     * @param ArrayObject $options
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        if (isset($data['colour']))
        {
            $hex = strtolower($data['colour']);
            $data['colour'] = preg_replace('/[^a-f0-9?!]/', '', $hex);
        }
    }

    /**
     * Finder for categories including uncompleted tasks
     * 
     * @param Query $query
     * @param array $options
     */
    public function findOpen(Query $query, array $options)
    {
        return $query->order(['priority' => 'desc'])
                        ->contain(['Tasks' => function ($q) use ($options) {
                                return $q
                                        ->find('open')
                                        ->find('filter', $options);
                            }]);
    }

    /**
     * Finder for categories including counts of tasks
     * 
     * @param Query $query
     * @param array $options
     */
    public function findTasksCount(Query $query, array $options)
    {
        return $query
                        ->select($this)
                        ->select(['tasks_completed' => 'SUM(CASE WHEN Tasks.completed IS NOT NULL THEN 1 END)'])
                        ->select(['tasks_open' => 'SUM(CASE WHEN Tasks.completed IS NULL THEN 1 END)'])
                        ->leftJoinWith('Tasks')
                        ->group('Categories.id');
    }

}
