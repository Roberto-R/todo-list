<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;

/**
 * Tasks Model
 *
 * @property \App\Model\Table\CategoriesTable&\Cake\ORM\Association\BelongsTo $Categories
 * @property \App\Model\Table\TagsTable&\Cake\ORM\Association\BelongsToMany $Tags
 *
 * @method \App\Model\Entity\Task get($primaryKey, $options = [])
 * @method \App\Model\Entity\Task newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Task[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Task|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Task saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Task patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Task[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Task findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TasksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tasks');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('NullifyText', ['fields' => [
                'description'
        ]]);

        $this->belongsTo('Categories');
        $this->belongsToMany('Tags');
        $this->hasMany('TagsTasks')
                ->setDependent(true); // Enable cascading delete
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->scalar('category_id')
                ->requirePresence('category_id', 'create');

        $validator
                ->scalar('name')
                ->maxLength('name', 200)
                ->requirePresence('name', 'create')
                ->notEmptyString('name');

        $validator
                ->scalar('completed')
                ->dateTime('completed');

        $validator
                ->scalar('description')
                ->allowEmptyString('description');

        $validator
                ->nonNegativeInteger('priority')
                ->allowEmptyString('priority');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['category_id'], 'Categories'));

        return $rules;
    }

    /**
     * Finder for all uncompleted tasks
     * 
     * @param Query $query
     * @param array $options
     */
    public function findOpen(Query $query, array $options)
    {
        return $query->where('Tasks.completed IS NULL')
                        ->order(['Tasks.priority' => 'desc']);
    }

    /**
     * Finder for all tasks based on filter fom
     * 
     * @param Query $query
     * @param array $options
     */
    public function findFilter(Query $query, array $options)
    {
        if (!isset($options['filter']) || !$options['filter'])
        {
            return $query;
        }
        
        $filter = $options['filter'];
        
        if (isset($filter['priority']) && $filter['priority'])
        {
            $query->where(['Tasks.priority' => $filter['priority']]);
        }
        
        if (isset($filter['tag_id']) && $filter['tag_id'])
        {
            $query->matching('Tags', function ($q) use ($filter) {
                return $q->where(['Tags.id' => $filter['tag_id']]);
            });
        }
        
        return $query;
    }

    /**
     * Finder for all completed tasks
     * 
     * @param Query $query
     * @param array $options
     */
    public function findArchive(Query $query, array $options)
    {
        return $query
                        ->select($this)
                        ->select(['category_name' => 'Categories.name'])
                        ->where('Tasks.completed IS NOT NULL')
                        ->innerJoinWith('Categories', function ($q) {
                            return $q->find('all', ['archive' => true]);
                        });
    }

}
