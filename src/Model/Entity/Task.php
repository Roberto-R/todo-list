<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Task Entity
 *
 * @property int $id
 * @property int $category_id
 * @property datetime $completed
 * @property string $name
 * @property string|null $description
 * @property int|null $priority
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Tag[] $tags
 */
class Task extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'category_id' => true,
        'name' => true,
        'completed' => true,
        'description' => true,
        'priority' => true,
        'created' => true,
        'modified' => true,
        'category' => true,
        'tags' => true,
    ];
    
    /**
     * Get formatted version of completed field
     * 
     * @return string
     */
    protected function _getCompletedFormat()
    {
        $completed = $this->_properties['completed'];
        
        if ($completed)
        {
            return $completed;
        }
        return '<i>No</i>';
    }

}
