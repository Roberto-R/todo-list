<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Category Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string $colour
 * @property int $priority
 * @property \Cake\I18n\FrozenTime|null $deleted
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Task[] $tasks
 */
class Category extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'colour' => true,
        'priority' => true,
        'created' => true,
        'modified' => true,
        'tasks' => true,
    ];

    /**
     * Check if deleted
     * 
     * @return bool
     */
    protected function _getIsDeleted()
    {
        return !is_null($this->_properties['deleted']);
    }

}
