<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tag Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string $colour
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Task[] $tasks
 */
class Tag extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'colour' => true,
        'created' => true,
        'modified' => true,
        'tasks' => true,
    ];
    
    /**
     * Return coloured div block
     * 
     * @return string
     */
    protected function _getColourFormat()
    {
        $colour = $this->_properties['colour'];
        
        if (!$colour)
        {
            return '<i>None</i>';
        }
        
        return '<div style="background-color: #' . $colour . '; height: 1rem; width: 2rem;"></div>';
    }

}
