<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\Event\Event;
use ArrayObject;

/**
 * NullifyText behaviour
 * 
 * Simple behaviour that turns empty strings into NULL before saving 
 * to the database.
 */
class NullifyTextBehavior extends Behavior
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'fields' => []
    ];
    
    /**
     * Parse data before entity creation
     * 
     * @param Event $event
     * @param ArrayObject $data
     * @param ArrayObject $options
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        foreach ($this->getConfig('fields') as $field)
        {
            if (isset($data[$field]))
            {
                // Replace empty string by null
                if (trim($data[$field]) == '')
                {
                    $data[$field] = null;
                }
            }
        }
    }
}
