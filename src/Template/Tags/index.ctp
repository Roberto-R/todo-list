<?php
$this->assign('current_menu', 'Tags');
?>

<h1>Tags</h1>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('Tags.name', 'Name') ?></th>
                <th><?= $this->Paginator->sort('Tags.colour', 'Colour') ?></th>
                <th><?= $this->Paginator->sort('count_tasks', 'Number of tasks') ?></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tags as $tag): ?>
                <tr>
                    <td><?= h($tag->name) ?></td>
                    <td><?= $tag->colour_format ?></td>
                    <td><?= $tag->count_tasks ?></td>
                    <td>
                        <?=
                        $this->Html->link(
                                '<i class="fas fa-edit"></i>',
                                ['action' => 'edit', $tag->id],
                                ['escape' => false])
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="button-panel">
    <?=
    $this->Html->link(
            '<i class="fas fas-plus"></i>&nbsp;Add Tag',
            ['action' => 'edit'],
            ['class' => 'btn btn-outline-info', 'escape' => false])
    ?>
</div>