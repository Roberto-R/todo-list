<h1>
    <?= $tag->isNew() ? 'Create new tage' : 'Edit tag' ?>
</h1>

<?= $this->Form->create($tag) ?>
<?= $this->Form->control('name') ?>
<?= $this->Form->control('description', ['type' => 'textarea', 'rows' => 1]) ?>
<?= $this->Form->control('colour', ['type' => 'color', 'value' => $tag->colour ? ('#' . $tag->colour) : '#FFFFFF']) ?>
<?= $this->Form->submit('Save') ?>
<?= $this->Form->end() ?>

<div class="button-panel">
    <?= $this->Html->link('Cancel', ['action' => 'index'], ['class' => 'btn btn-outline-secondary']) ?>

    <?php
    if (!$tag->isNew())
    {
        echo $this->Form->postLink(
                '<i class="fas fa-trash"></i>&nbsp;Delete',
                ['action' => 'delete', $tag->id],
                ['class' => 'btn btn-outline-danger', 'escape' => false, 'confirm' => 'Delete this tag?']);
    }
    ?>
</div>