<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            TODO LIST
        </title>
        <?= $this->Html->meta('icon') ?>

        <?= $this->Html->css('cake.css') ?>
        <?= $this->Html->css('bootstrap-4.3.1.min.css') ?>
        <?= $this->Html->css('fontawesome-all-5.11.2.min.css') ?>
        <?= $this->Html->css('jquery-ui-1.12.1.min.css') ?>
        <?= $this->Html->css('style.css') ?>

        <?= $this->Html->script('jquery-3.4.1.min.js') ?>
        <?= $this->Html->script('bootstrap-4.3.1.min.js') ?>
        <?= $this->Html->script('jquery-ui-1.12.1.min.js') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body>
        <?=
        $this->element('Layout/navbar', ['items' => [
                'Tasks' => ['controller' => 'Tasks', 'action' => 'index'],
                'Tags' => ['controller' => 'Tags', 'action' => 'index'],
                'Archive' => ['controller' => 'Tasks', 'action' => 'indexArchive']
            ]
        ])
        ?>

        <?= $this->Flash->render() ?>

        <div class="container clearfix">
            <?= $this->fetch('content') ?>
        </div>

    </body>
</html>
