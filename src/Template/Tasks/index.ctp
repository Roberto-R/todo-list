<?php
$this->assign('current_menu', 'Tasks');

$collapse = '';
if ($this->request->getQuery())
{
    $collapse = ' show';
}
?>

<h1>Open Tasks</h1>

<button class="btn btn-outline-primary" type="button" data-toggle="collapse" data-target="#filter-panel" aria-expanded="false" aria-controls="filter-panel">
    Filters >
</button>
<div class="collapse<?= $collapse ?>" id="filter-panel">
    <div class="card card-body">
        <?= $this->Form->create(null, ['type' => 'GET', 'valueSources' => ['query']]) ?>
        <?=
        $this->Form->control('priority', [
            'type' => 'select',
            'options' => [0 => 'Low', 1 => 'Medium', 2 => 'High', 3 => 'Critical'],
            'empty' => 'All'
        ])
        ?>
        <?=
        $this->Form->control('tag_id', [
            'type' => 'select',
            'options' => $tags_list,
            'empty' => 'All'
        ])
        ?>
        <?= $this->Form->submit('Search') ?>
        <?= $this->Form->end() ?>
    </div>
</div>
<br><br>

<div class="card-deck categories-deck">
    <?php foreach ($categories as $category) : ?>
        <?= $this->element('Categories/card', compact('category')) ?>
    <?php endforeach; ?>
</div>

<div class="button-panel">
    <?=
    $this->Html->link(
            '<i class="fas fa-plus"></i>&nbsp;New Category',
            ['controller' => 'Categories', 'action' => 'edit'],
            ['class' => 'btn btn-outline-info', 'escape' => false])
    ?>
</div>