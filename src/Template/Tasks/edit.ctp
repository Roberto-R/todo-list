<h1>
    <?= $task->isNew() ? 'Create new task' : 'Edit task' ?>
</h1>

<?php
$sources = ['context'];

if ($task->isNew())
{
    $sources[] = 'query';
}
?>
<?= $this->Form->create($task, ['valueSources' => $sources]) ?>
<?= $this->Form->control('name') ?>
<?= $this->Form->control('category_id') ?>
<?= $this->Form->control('tags._ids') ?>
<?= $this->Form->control('description', ['type' => 'textarea', 'rows' => 1]) ?>
<?=
$this->Form->control('priority', [
    'type' => 'select',
    'options' => [0 => 'Low', 1 => 'Medium', 2 => 'High', 3 => 'Critical']
])
?>
<?= $this->Form->submit('Save') ?>
<?= $this->Form->end() ?>

<div class="button-panel">
    <?= $this->Html->link('Cancel', '/', ['class' => 'btn btn-outline-secondary']) ?>

    <?php
    if (!$task->isNew())
    {
        echo $this->Form->postLink(
                '<i class="fas fa-trash"></i>&nbsp;Delete',
                ['action' => 'delete', $task->id],
                ['class' => 'btn btn-outline-danger', 'escape' => false, 'confirm' => 'Delete this task?']);
    }
    ?>
</div>