<h1>View Task</h1>

<?=
$this->Html->link(
        'Back',
        ['action' => 'index'],
        ['class' => 'btn btn-secondary'])
?>

<br><br>

<table class="table vertical-table">
    <tbody>
        <tr>
            <th>Name</th>
            <td><?= $task->name ?></td>
        </tr>
        <tr>
            <th>Description</th>
            <td><?= $task->description ?></td>
        </tr>
        <tr>
            <th>Category</th>
            <td><?= $task->category->name ?></td>
        </tr>
        <tr>
            <th>Completed</th>
            <td><?= $task->completed_format ?></td>
        </tr>
    </tbody>
</table>

<div class="button-panel">
    <?=
    $this->Html->link(
            '<i class="fas fa-edit"></i>&nbsp;Edit',
            ['action' => 'edit', $task->id],
            ['class' => 'btn btn-outline-info', 'escape' => false])
    ?>
</div>
