<?php
$this->assign('current_menu', 'Archive');
?>

<h1>Completed Tasks</h1>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('Tasks.name', 'Name') ?></th>
                <th><?= $this->Paginator->sort('Tasks.completed', 'Completed') ?></th>
                <th><?= $this->Paginator->sort('Tasks.priority', 'Priority') ?></th>
                <th><?= $this->Paginator->sort('Categories.name', 'Category') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tasks as $task): ?>
            <tr>
                <td><?= h($task->name) ?></td>
                <td><?= h($task->completed_format) ?></td>
                <td>P<?= $task->priority ?></td>
                <td><?= h($task->category_name) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?= $this->element('pagination_panel') ?>