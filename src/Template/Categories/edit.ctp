<h1>
    <?= $category->isNew() ? 'Create new category' : 'Edit category' ?>
</h1>

<?= $this->Form->create($category) ?>
<?= $this->Form->control('name') ?>
<?= $this->Form->control('description', ['type' => 'textarea', 'rows' => 1]) ?>
<?= $this->Form->control('colour', ['type' => 'color', 'value' => $category->colour ? ('#' . $category->colour) : '#FFFFFF']) ?>
<?= $this->Form->submit('Save') ?>
<?= $this->Form->end() ?>

<div class="button-panel">
    <?= $this->Html->link('Cancel', '/', ['class' => 'btn btn-outline-secondary']) ?>

    <?php
    if (!$category->isNew())
    {
        echo $this->Form->postLink(
                '<i class="fas fa-trash"></i>&nbsp;Delete',
                ['action' => 'delete', $category->id],
                ['class' => 'btn btn-outline-danger', 'escape' => false, 'confirm' => 'Delete this category AND underlying uncompleted tasks?']);
    }
    ?>
</div>