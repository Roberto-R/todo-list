<?php
$class = 'message alert alert-primary';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="<?= h($class) ?>" onclick="$(this).fadeOut(300);"><?= $message ?></div>
