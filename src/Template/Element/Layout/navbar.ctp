<script>
    $(document).ready(function () {

        $("#tasks-search").autocomplete({
            //source: "<?= $this->Url->build(['controller' => 'Tasks', 'action' => 'search', '_ext' => 'json']) ?>",
            source: function (request, response) {
                $.ajax({
                    url: "<?= $this->Url->build(['controller' => 'Tasks', 'action' => 'search', '_ext' => 'json']) ?>",
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    success: function (data) {

                        response(data.tasks);
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                window.location.replace("<?= $this->Url->build(['controller' => 'Tasks', 'action' => 'view']) ?>" + "/" + ui.item.id);
            }
        });
    });
</script>

<nav class="navbar navbar-expand-lg navbar-light bg-light">

    <?= $this->Html->link('TODO LIST', '/', ['class' => 'navbar-brand']) ?>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <ul class="navbar-nav mr-auto">

            <?php
            if (!isset($current_menu))
            {
                $current_menu = $this->fetch('current_menu');
            }

            foreach ($items as $name => $link) :

                $active = '';
                if ($current_menu == $name)
                {
                    $active = ' active';
                }
                ?>
                <li class="nav-item<?= $active ?>">
                    <?= $this->Html->link($name, $link, ['class' => 'nav-link']) ?>
                </li>
            <?php endforeach; ?>
        </ul>
        
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" id="tasks-search" type="search" placeholder="Search for task" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="button">Search</button>
        </form>
    </div>
</nav>