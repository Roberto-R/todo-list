<div class="card" style="background-color: #<?= $category->colour ?>">
    <div class="card-header">
        <?= h($category->name) ?>

        <span class="float-right">
            <?=
            $this->Html->link(
                    '<i class="fas fa-edit"></i>',
                    ['controller' => 'Categories', 'action' => 'edit', $category->id],
                    ['class' => 'btn btn-sm', 'escape' => false])
            ?>
        </span>
    </div>

    <?php if ($category->tasks) : ?>
        <ul class="list-group list-group-flush">
            <?php foreach ($category->tasks as $task) : ?>
                <li class="list-group-item">

                    <?php if ($task->tags) : ?>
                            <?php foreach ($task->tags as $tag) : ?>
                                <span class="badge" style="background-color: #<?= $tag->colour ?>;"><?= $tag->name ?></span>
                            <?php endforeach; ?>
                        <br>
                    <?php endif; ?>

                    <?=
                    $this->Form->postLink(
                            '<i class="far fa-check-square"></i>',
                            ['action' => 'complete', $task->id],
                            ['class' => 'btn btn-sm', 'escape' => false, 'confirm' => 'Complete this task?'])
                    ?>

                    <span>

                        <?= h($task->name) ?>

                        <?php
                        if ($task->priority > 0) :
                            if ($task->priority == 1)
                            {
                                $class = 'info';
                            }
                            elseif ($task->priority == 2)
                            {
                                $class = 'warning';
                            }
                            else
                            {
                                $class = 'danger';
                            }
                            ?>
                            &nbsp;<span class="badge badge-<?= $class ?>">P<?= $task->priority ?></span>
                        <?php endif; ?>

                    </span>

                    <span class="float-right">
                        <?=
                        $this->Html->link(
                                '<i class="fas fa-external-link-alt"></i>',
                                ['action' => 'view', $task->id],
                                ['class' => 'btn btn-sm', 'escape' => false])
                        ?>
                    </span>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    <div class="card-footer">
        <?=
        $this->Html->link(
                '<i class="fas fa-plus"></i>&nbsp;New Task',
                ['action' => 'edit', '?' => ['category_id' => $category->id]],
                ['class' => 'btn btn-sm', 'escape' => false])
        ?>
    </div>

</div>