<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Exception\BadRequestException;
use Cake\I18n\FrozenTime;

/**
 * Tasks Controller
 *
 * @property \App\Model\Table\TasksTable $Tasks
 *
 * @method \App\Model\Entity\Task[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TasksController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $categories = $this->Tasks->Categories->find('open', ['filter' => $this->request->getQuery()])
                ->contain('Tasks.Tags');

        $tags_list = $this->Tasks->Tags->find('list', ['limit' => 200]);
        
        $this->set(compact('categories', 'tags_list'));
    }

    /**
     * Archive page with all completed tasks
     */
    public function indexArchive()
    {
        $this->paginate = [
            'limit' => 20,
            'order' => [
                'Tasks.completed' => 'desc'
            ],
            'sortWhitelist' => ['Tasks.name', 'Tasks.completed', 'Categories.name', 'Tasks.priority']
        ];

        $tasks = $this->Tasks->find('archive');

        $this->set('tasks', $this->paginate($tasks));
    }

    /**
     * View method
     *
     * @param string|null $id Task id
     */
    public function view($id = null)
    {
        $task = $this->Tasks->get($id, [
            'contain' => ['Categories'],
        ]);

        $this->set(compact('task'));
    }

    /**
     * Combined add and edit method
     *
     * @param string|null $id Task id
     */
    public function edit($id = null)
    {
        if ($id)
        {
            $task = $this->Tasks->get($id);
        }
        else
        {
            $task = $this->Tasks->newEntity();
        }

        if ($this->request->is(['patch', 'post', 'put']))
        {
            $this->Tasks->patchEntity($task, $this->request->getData());

            if ($this->Tasks->save($task))
            {
                $this->Flash->success('The task has been saved');

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error('The task could not be saved, please try again');
        }

        $categories = $this->Tasks->Categories->find('list', ['limit' => 200]);
        $tags = $this->Tasks->Tags->find('list', ['limit' => 200]);
        $this->set(compact('task', 'categories', 'tags'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Task id
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $task = $this->Tasks->get($id);

        if ($this->Tasks->delete($task))
        {
            $this->Flash->success('The task has been deleted');
        }
        else
        {
            $this->Flash->error('The task could not be deleted. Please, try again.');
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Search for tasks (JSON page)
     * 
     * Use 'term' query parameters for search query
     */
    public function search()
    {
        $q = $this->request->getQuery('term', false);

        if (!$q)
        {
            throw new BadRequestException('Missing the `term` search query');
        }

        $tasks = $this->Tasks->find()
                ->select(['id' => 'id', 'label' => 'name', 'value' => 'name'])
                ->where('completed IS NULL')
                ->where(['OR' => [
                        ['name LIKE' => "%{$q}%"],
                        ['description LIKE' => "%{$q}%"]
            ]])
                ->disableHydration();

        $this->set(compact('tasks'));
    }

    /**
     * Complete a task
     * 
     * @param int|string $id Task id
     */
    public function complete($id = null)
    {
        $this->request->allowMethod(['post', 'patch']);

        $task = $this->Tasks->get($id);

        $task->completed = new FrozenTime();

        if ($this->Tasks->save($task))
        {
            $this->Flash->success('Completed task!');
        }
        else
        {
            $this->Flash->error('Failed to complete task...');
        }

        return $this->redirect('/');
    }

}
