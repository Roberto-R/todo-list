<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Tags Controller
 *
 * @property \App\Model\Table\TagsTable $Tags
 *
 * @method \App\Model\Entity\Tag[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TagsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'limit' => 100,
            'order' => [
                'Tags.name' => 'asc'
            ],
            'sortWhitelist' => ['Tags.name', 'Tags.colour', 'count_tasks']
        ];
        
        $tags = $this->Tags->find('tasksCount');

        $this->set('tags', $this->paginate($tags));
    }

    /**
     * Edit method
     *
     * @param string|null $id Tag id
     */
    public function edit($id = null)
    {
        if ($id)
        {
            $tag = $this->Tags->get($id);
        }
        else
        {
            $tag = $this->Tags->newEntity();
        }

        if ($this->request->is(['patch', 'post', 'put']))
        {
            $this->Tags->patchEntity($tag, $this->request->getData());
            
            if ($this->Tags->save($tag))
            {
                $this->Flash->success('The tag has been saved!');

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error('The tag could not be saved. Please, try again.');
        }
        
        $this->set(compact('tag'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Tag id
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        
        $tag = $this->Tags->get($id);
        
        if ($this->Tags->delete($tag))
        {
            $this->Flash->success('The tag has been deleted');
        }
        else
        {
            $this->Flash->error('The tag could not be deleted. Please, try again.');
        }

        return $this->redirect(['action' => 'index']);
    }

}
