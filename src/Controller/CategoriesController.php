<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\FrozenTime;

/**
 * Categories Controller
 *
 * @property \App\Model\Table\CategoriesTable $Categories
 *
 * @method \App\Model\Entity\Category[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CategoriesController extends AppController
{

    /**
     * Combined add and edit method
     *
     * @param string|null $id Category id
     */
    public function edit($id = null)
    {   
        if ($id)
        {
            $category = $this->Categories->get($id);
        }
        else
        {
            $category = $this->Categories->newEntity();
        }
        
        
        if ($this->request->is(['patch', 'post', 'put']))
        {
            $this->Categories->patchEntity($category, $this->request->getData());
            
            if ($this->Categories->save($category))
            {
                $this->Flash->success('The category has been saved!');

                return $this->redirect('/');
            }
            $this->Flash->error('The category could not be saved, please try again');
        }
        
        $this->set(compact('category'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Category id
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        
        $category = $this->Categories->get($id, [
            'finder' => 'tasksCount'
        ]);
        
        $success = false;
        
        if ($category->tasks_completed == 0)
        {
            // No archive stuff, so hard delete
            $success = (bool) $this->Categories->delete($category);
        }
        else
        {
            $category->deleted = new FrozenTime();
            $success = (bool) $this->Categories->save($category);
        }
        
        if ($success)
        {
            $this->Flash->success("The category has been deleted");
        }
        else
        {
            $this->Flash->error('The category could not be deleted. Please, try again.');
        }

        return $this->redirect('/');
    }

}
